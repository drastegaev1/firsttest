package Patterns;

import lombok.*;

@lombok.Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString

public class Builder {
    private String a, b, c, d, e, f, g, h, l, m, n;

}
