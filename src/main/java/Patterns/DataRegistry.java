package Patterns;

import java.util.concurrent.atomic.AtomicInteger;

public class DataRegistry {
 /*   private static AtomicInteger COUNTER = new AtomicInteger(0);

    public static String getUser(){
        int index = COUNTER.incrementAndGet();
        return "User" + index;
    }
    */
   private static int COUNTER = 0;

    public static String getUser() {
        int index = ++COUNTER;
        return "User" + index;
    }


}
