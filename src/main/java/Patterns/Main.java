package Patterns;

public class Main {
    public static void main(String[] args) {

        Singleton singleton = Singleton.getInstance();          // паттерн Singleton
        Singleton singleton1 = Singleton.getInstance();
        Singleton singleton2 = Singleton.getInstance();

        singleton.setA("Hello world");
        System.out.println(singleton1.getA());
        System.out.println(singleton2.getA());



        Builder builder = Builder.builder()                     // паттерн Builder
                .a("a")
                .b("b")
                .build();
        System.out.println(builder.getA() + builder.getB());

        Builder builder1 = Builder.builder()
                .b("b")
                .d("d")
                .build();
        System.out.println(builder1.getB() + builder1.getD());

        System.out.println(DataRegistry.getUser());          // паттерн DataRegistry
        System.out.println(DataRegistry.getUser());
        System.out.println(DataRegistry.getUser());

        ObjectPool.setUser("User11");               // Заведение значений переменных паттерна ObjectPool
        ObjectPool.setUser("User22");
        ObjectPool.setUser("User33");
        ObjectPool.setUser("User44");

        for (int i = 0; i <= 10; i++) {             // Логика к работе паттерна ObjectPool
            String user = ObjectPool.getUser();         // Вывод значений паттерна ObjectPool
            System.out.println(user);
            ObjectPool.setUser(user);


        }
        SomeMethod("1", "2", "3", "4", "5");                                //заведение значений переменных паттерн ObjectPool
        ValueObject object = new ValueObject("1", "2", "3", "4", "5");      //заведение значений переменных в объект паттерн ObjectPool
        SomeMethodValueObject(object);
    }

    public static void SomeMethod(String a, String b, String c, String d, String e) {       // вывод переменных паттерн ObjectPool
        System.out.println(a + b + c + d + e);
    }

    public static void SomeMethodValueObject(ValueObject o) {                           // вывод переменных из созданного объекта паттерн ObjectPool
        System.out.println(o.getA() + o.getB() + o.getC() + o.getD() + o.getE());
    }


}

