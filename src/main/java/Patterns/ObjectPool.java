package Patterns;

import java.util.ArrayDeque;

public class ObjectPool {
    private static ArrayDeque<String> users = new ArrayDeque<>();

    public static String getUser() {
        return users.poll();
    }

    public static void setUser(String user) {
        users.addLast(user);
    }
}
