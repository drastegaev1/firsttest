import Animals.Animal;
import Animals.Dog;
import Animals.Cat;

public class Main2 {
    public static void main(String[] args){
        Dog dog = new Dog();
        dog.setAge(12);
        dog.setName("Тоник");
        dog.Say();

        Cat cat = new Cat();
        cat.setAge(9);
        cat.setName("Пуша");
        cat.Say();

        Animal animal = new Animal();
        animal.Say();

    }

}
