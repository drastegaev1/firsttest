package TryLesson;

public class Main {
    public static void main(String[] args) {
        int[] array = new int[3];
        try {
            array[4] = 3;
        } catch (Exception ex) {
            //       ex.printStackTrace();
            System.out.println("Вы вышли за границы массива");
        }
        System.out.println("End");

        try {
            int i = sum(3, 3);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();

        }

        try {
            System.out.println(sum(4, -2));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int sum(int a, int b) throws Exception {
        if ((a < 0) || (b < 0)) throw new Exception("Must be more than 0");
        return a + b;
    }

}
