package CollectionLesson;

import java.util.HashMap;

public class Main4 {
    public static void main(String[] args) {
        HashMap<String, String> map = new HashMap<String, String>(); // Формирование типа словаря (первый элемент ключ)
        map.put("Hello", "Привет"); //элемент словаря
        map.put("World", "Мир");
        map.put("War", "Война");

        System.out.println(map.get("Hello") + " " + map.get("World"));
        System.out.println(map.get("Война"));
    }
}
