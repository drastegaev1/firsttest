package CollectionLesson;

import java.util.ArrayDeque;

public class Main2 {
    public static void main(String[] args) {
        ArrayDeque<String> eque = new ArrayDeque<String>(); // формирование массива очереди

        eque.addLast("Включи компьютер"); // элемент очереди
        eque.addLast("Зайди BIOS"); // элемент очереди
        eque.addLast("Поменять приоритет"); // элемент очереди

        System.out.println(eque); // вывод очереди

        //    Integer i = 0;

        while (!eque.isEmpty()) {
            String el = eque.pollFirst(); // забираем первый элемент в переменную el
            System.out.println(el); // вывод элементов очереди по элементно
            //      i++;
            //    eque.addLast(i.toString());
        }

        System.out.println(eque);
    }
}
