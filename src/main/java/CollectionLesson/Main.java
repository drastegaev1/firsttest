package CollectionLesson;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> list1 = new ArrayList<String>(); //добавление списка
        ArrayList<String> list2 = new ArrayList<String>(list1); //добавление списка родителя
        ArrayList<String> list3 = new ArrayList<String>(30); //добавление списка с определением кол-ва элементов, при этом это не ограничивает кол-во элементов

        list1.add("Привет"); // добавление 1 элемента
        list1.add(0, "World"); //

        list1.set(1, "Hello");

        System.out.println(list1);
        System.out.println(list1.isEmpty());

        String result = list1.get(1) + " " + list1.get(0);

        System.out.println(result);

        list1.remove("World");

        for (String el : list1) {
            System.out.println(el);

        }
    }
}
