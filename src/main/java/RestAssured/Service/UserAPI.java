package RestAssured.Service;

import RestAssured.DTO.User;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given; // обязательно подключить для работы given


public class UserAPI {
    private final String BASE_URL = "https://petstore.swagger.io/v2";
    private final String USER = "/user";
 /*   private RequestSpecification spec;

    public class UserAPI(String baseUrl){
        spec = given()
        .baseUri(baseUrl)
             .contentType(ContentType.JSON)

    }

  */

    public Response createUser(User user) {
        return given()
                .baseUri(BASE_URL)
                .contentType(ContentType.JSON)
                .with()
                .body(user)
                .log().all()
                .when()
                .post(USER);

  /*  }
    public Response getUser(){
        return given()
                .baseUri(BASE_URL)
                .contentType(ContentType.JSON)
                .with()
                .body(user)
                .log().all()
                .when()
                .post(USER);
    }
*/
    }
}
