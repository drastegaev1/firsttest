package RestAssured.DTO;

import lombok.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonSerialize
@EqualsAndHashCode

public class UserOUT {
    private int code;
    private String type;
    private String message;
}
