package swagger;

import com.atlassian.oai.validator.restassured.OpenApiValidationFilter;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static io.restassured.RestAssured.given;

public class OpenApiValidationTest {
    private static final int PORT = 5555;
    private static final String SWAGGER_JSON_URL = "https://petstore.swagger.io/v2/swagger.json";

    private final OpenApiValidationFilter validationFilter = new OpenApiValidationFilter(SWAGGER_JSON_URL);


    @Rule
    public WireMockRule wireMockRule = new WireMockRule(PORT); // @Rule анатоция junit 4 версии

    @Before
    public void setup() {

        wireMockRule.stubFor(
                WireMock.get(urlEqualTo("/pet/1"))  // данные берем из https://petstore.swagger.io/#/pet/getPetById
                        .willReturn(aResponse()
                                .withStatus(200)
                                .withHeader("content-type", "application/json")
                                .withBody("{\"name\":\"fido\", \"photoUrls\":[]}")));

        wireMockRule.stubFor(
                WireMock.get(urlEqualTo("/pet/2"))
                        .willReturn(aResponse()
                                .withStatus(200)
                                .withHeader("content-type", "application/json")
                                .withBody("{\"name\":\"fido\"}"))); // Missing required 'photoUrls' field

        wireMockRule.stubFor(
                WireMock.get(urlEqualTo("/pet/fido")) // Invalid petId
                        .willReturn(aResponse()
                                .withStatus(200)
                                .withHeader("content-type", "application/json")
                                .withBody("{\"name\":\"fido\", \"photoUrls\":[]}")));
    }

    @Test
    public void testGetValidPet() {
        given().port(PORT)
                //.baseUri("http://petstore.swagger.io/v2")
                .filter(validationFilter)
                .header("api_key", "foo")
                .log().all()
                .when()
                .get("/pet/1")
                .then()
                .assertThat()
                .log().all()
                .statusCode(200);
    }

    @Test
    public void testGetValidPet2() {
        given().port(PORT)
                //.baseUri("http://petstore.swagger.io/v2")
                .filter(validationFilter)
                .header("api_key", "foo")
                .log().all()
                .when()
                .get("/pet/2")
                .then()
                .assertThat()
                .log().all()
                .statusCode(200);
    }
    @Test
    public void testGetValidPet3() {
        given().port(PORT)
                //.baseUri("http://petstore.swagger.io/v2")
                .filter(validationFilter)
                .header("api_key", "foo")
                .log().all()
                .when()
                .get("/pet/fido")
                .then()
                .assertThat()
                .log().all()
                .statusCode(200);
    }
}